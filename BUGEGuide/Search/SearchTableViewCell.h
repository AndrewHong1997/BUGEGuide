//
//  SearchTableViewCell.h
//  BUGEGuide
//
//  Created by Andrew Leung on 1/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SearchTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *courseCodeLabel;

@end

