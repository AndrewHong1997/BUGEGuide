//
//  SearchTableViewController.m
//  BUGEGuide
//
//  Created by Andrew Leung on 1/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "SearchTableViewController.h"

@interface SearchTableViewController ()

@end

@implementation SearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self setupSearchViewController];
    self.courseDict = [[NSMutableDictionary alloc] init];

}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}



-(void) setupSearchViewController {
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = YES;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchController.searchBar.placeholder = @"Search By Course Code";
    self.searchController.searchBar.barTintColor = UIColor.clearColor;
    self.searchController.searchBar.backgroundColor = UIColor.whiteColor;
    
    if(@available(iOS 11.0, *)){
        self.navigationItem.searchController = self.searchController;
        self.searchTabelView.tableHeaderView = nil;
    }else{
        self.searchTabelView.tableHeaderView = self.searchController.searchBar;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchTableViewCell"];
    if(!cell){
        [[NSBundle mainBundle] loadNibNamed:@"SearchTableViewCell" owner:self options:nil];
        cell = _Acell;
        _Acell = nil;
    }
    
    if(self.searchController.active){
        cell.courseCodeLabel.text = [[self.searchResultArray objectAtIndex:indexPath.row] uppercaseString];
    }
    
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(self.searchController.active){
        return [self.searchResultArray count];
    }
    return 0;
    
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.searchController.active){
        if(!self.courseDetailInfoViewController)
            self.courseDetailInfoViewController = [[CourseDetailInfoViewController alloc] initWithNibName:@"CourseDetailInfoViewController" bundle:nil];
        NSString *selectedCourseID;
        if(self.searchController.active){
            selectedCourseID = [self.searchResultArray objectAtIndex:indexPath.row];
        }
        
        NSDictionary *cellDict = [self.courseDict objectForKey:selectedCourseID];
        self.courseDetailInfoViewController.delegate = self;
        self.courseDetailInfoViewController.courseInfoDict = cellDict;
        
        NSString *courseCode = [[cellDict objectForKey:@"courseCode"] uppercaseString];
        NSCharacterSet *removeChar = [NSCharacterSet characterSetWithCharactersInString:@"_"];
        NSString *resultCourseCode = [[courseCode componentsSeparatedByCharactersInSet:removeChar] componentsJoinedByString:@"/"];
        self.courseDetailInfoViewController.title = resultCourseCode;
        [self.navigationController pushViewController:self.courseDetailInfoViewController animated:YES];
    }
}

#pragma mark - UISearchBar delegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    self.searchResultArray = [[NSArray alloc] init];
    self.isSearchEnabled = YES;
    NSPredicate *coursePredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS [cd] %@",self.searchController.searchBar.text];
    self.searchResultArray = [self.courseIDArray filteredArrayUsingPredicate:coursePredicate];
    self.searchTabelView.hidden = false;
    [self.searchTabelView reloadData];
}


@end
