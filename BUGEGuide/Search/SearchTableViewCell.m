//
//  SearchTableViewCell.m
//  BUGEGuide
//
//  Created by Andrew Leung on 1/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "SearchTableViewCell.h"

@implementation SearchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
