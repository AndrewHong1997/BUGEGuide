//
//  SearchTableViewController.h
//  BUGEGuide
//
//  Created by Andrew Leung on 1/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchTableViewCell.h"
#import "GeneralNetworkManager.h"
#import "CourseDetailInfoViewController.h"

@protocol SearchTableViewControllerDelegate;

@interface SearchTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CourseDetailInfoViewControllerDelegate,UISearchControllerDelegate,UISearchResultsUpdating>

@property (nonatomic, retain) UISearchController *searchController;
@property (nonatomic, retain) CourseDetailInfoViewController *courseDetailInfoViewController;
@property (nonatomic, assign) id<SearchTableViewControllerDelegate>delegate;
@property (nonatomic, retain) IBOutlet UITableView *searchTabelView;
@property (nonatomic, retain) IBOutlet SearchTableViewCell *Acell;
@property (nonatomic, retain) NSArray *courseIDArray;
@property (nonatomic, retain) NSArray *searchResultArray;
@property (nonatomic, retain) NSMutableDictionary *courseDict;
@property BOOL isSearchEnabled;


@end

@protocol SearchTableViewControllerDelegate<NSObject>
@end
