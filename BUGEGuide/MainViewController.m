//
//  MainViewController.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 6/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import <GoogleSignIn/GoogleSignIn.h>


@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    self.view.layer.backgroundColor = [UIColor colorWithRed: 68.0/255.0 green: 178.0/255.0 blue: 253.0/255.0 alpha: 1.0].CGColor;
    [self customizeButtonLayer:self.googleLoginButton];

    
}

-(void) customizeButtonLayer:(UIButton *)button {
    
    button.layer.cornerRadius = 16.0f;
    button.layer.cornerRadius = 10;
    button.layer.shadowColor = UIColor.blackColor.CGColor;
    button.layer.shadowOpacity = 0.2;
    button.layer.shadowRadius = 6.0;
    button.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    button.layer.masksToBounds = NO;
    [button setTitleColor:[UIColor colorWithRed: 124.0/255.0 green: 124.0/255.0 blue: 124.0/255.0 alpha: 1.0] forState:UIControlStateNormal];
}

- (IBAction)GoogleSignInPressed:(id)sender {
    
    [[GIDSignIn sharedInstance] signIn];
}



@end
