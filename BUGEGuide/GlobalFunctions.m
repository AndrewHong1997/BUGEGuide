//
//  GlobalFunctions.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 9/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "GlobalFunctions.h"

static UIView *overlayView = nil;
static UIActivityIndicatorView *spinner = nil;

@implementation GlobalFunctions

+ (void) addOverlayToView: (UIView *)view{
    // Add overlay
    overlayView = [[UIView alloc] initWithFrame:view.bounds];
    [overlayView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    overlayView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
    spinner = [[UIActivityIndicatorView alloc]
               initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    CGRect frame = spinner.frame;
    frame.origin.x = overlayView.frame.size.width / 2 - frame.size.width / 2;
    frame.origin.y = overlayView.frame.size.height / 2 - frame.size.height / 2;
    spinner.frame = frame;
    [spinner setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin];
    [overlayView addSubview:spinner];
    [view addSubview:overlayView];
    [spinner startAnimating];
}

+ (void) removeOverlay{
    [spinner stopAnimating];
    [overlayView removeFromSuperview];
    overlayView = nil;
    spinner = nil;
}


@end
