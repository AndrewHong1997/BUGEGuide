//
//  MainViewController.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 6/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>


@interface MainViewController : UIViewController<GIDSignInUIDelegate>
@property (retain, nonatomic) IBOutlet UIButton *googleLoginButton;

@end


