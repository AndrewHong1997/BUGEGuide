//
//  GeneralNetworkManager.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 10/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "Firebase.h"

@interface GeneralNetworkManager : NSObject

@property (nonatomic,retain) Reachability *hostReachability;
@property (nonatomic,retain) FIRDatabaseReference *ref;


+ (GeneralNetworkManager *)manager;
-(void) getAllCourseWithCompletionHander :(void (^) (NSDictionary *courseDict ,NSError *error))completionHandler;
-(void) getCommentsWithCompletionHandler:(void(^)(NSDictionary*commentDict,NSError *errror))completionHandler;


@end

