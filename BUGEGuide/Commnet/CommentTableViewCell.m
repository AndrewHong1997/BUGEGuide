//
//  CommentTableViewCell.m
//  BUGEGuide
//
//  Created by Andrew Leung on 3/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "CommentTableViewCell.h"

@implementation CommentTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    // Initialization code
    
    self.layer.shadowColor = UIColor.blackColor.CGColor;
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowRadius = 3.0;
    self.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.layer.masksToBounds = NO;
    
    self.gradeLabel.layer.cornerRadius = self.gradeLabel.frame.size.height/2;
    self.gradeLabel.clipsToBounds = YES;
    self.gradeLabel.layer.masksToBounds = YES;
    
    
}

-(void) setFrame:(CGRect)frame{
    frame.origin.x +=10;
    frame.size.width -=2 *10;
    [super setFrame:frame];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

@end
