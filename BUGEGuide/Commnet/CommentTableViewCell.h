//
//  CommentTableViewCell.h
//  BUGEGuide
//
//  Created by Andrew Leung on 3/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface CommentTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *academicYearLabel;
@property (retain, nonatomic) IBOutlet UILabel *tutorNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *gradeLabel;
@property (retain, nonatomic) IBOutlet UITextView *commentTextView;
@property (retain, nonatomic) IBOutlet UILabel *timeStampLabel;
@property (retain, nonatomic) IBOutlet UILabel *suggestLabel;
@property (retain, nonatomic) IBOutlet UILabel *workLoadLabel;
@property (retain, nonatomic) IBOutlet HCSStarRatingView *starRatingView;

@end

