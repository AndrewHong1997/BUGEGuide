//
//  CommentTextViewController.m
//  BUGEGuide
//
//  Created by Andrew Leung on 4/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "CommentTextViewController.h"

@interface CommentTextViewController ()

@end

@implementation CommentTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupToolBar];
    
    self.title = @"Comment";
    
    self.scrollView.frame = CGRectMake(0, 69, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.scrollView];
    self.scrollView.scrollEnabled = true;
    
    UIBarButtonItem *postButton = [[UIBarButtonItem alloc] initWithTitle:@"Post" style:UIBarButtonItemStyleDone target:self action:@selector(postButtonPressed)];
    self.navigationItem.rightBarButtonItem = postButton;
    
    self.ref = [[FIRDatabase database] reference];
}

-(void) viewWillAppear:(BOOL)animated{
     self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height+50);
    self.textView.text = @"";
}


-(void) didReceiveMemoryWarning{
    
}

#pragma mark - UITextViewDelegation method

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
}

-(void) textViewDidEndEditing:(UITextView *)textView{
    
    [textView resignFirstResponder];
}

-(void) setupToolBar {
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    [toolBar sizeToFit];
    self.textView.inputAccessoryView = toolBar;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissKeyboard)];
    toolBar.items = [NSArray arrayWithObjects:doneButton, nil];
}

-(void) dismissKeyboard {
    [self.textView resignFirstResponder];
}

-(void) showCommentSuccessfulAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Upload Successful" message:@"Your comment uploaded successfully" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) postButtonPressed {
    
    if(self.textView.text !=nil){
        [GlobalFunctions addOverlayToView:self.view];
        [self.uploadCommentDict setObject:self.textView.text forKey:@"commentContent"];
        [self.uploadCommentDict setObject:FIRServerValue.timestamp forKey:@"createTime"];
        NSString *key = [[self.ref child:@"Comments"] childByAutoId].key;
        NSDictionary *postUpdate = @{[NSString stringWithFormat:@"/user-posts/%@/%@/",self.courseID, key]: self.uploadCommentDict};
        [self.ref updateChildValues:postUpdate withCompletionBlock:^(NSError *error, FIRDatabaseReference *ref) {
            if(error){
                NSLog(@"%@",error.localizedDescription);
            }else{
                dispatch_async(dispatch_get_main_queue(),^{
                    [GlobalFunctions removeOverlay];
                    [self showCommentSuccessfulAlert];
                });
            }
        }];
    }
}

@end
