//
//  CreateCommentViewController.m
//  BUGEGuide
//
//  Created by Andrew Leung on 3/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "CreateCommentViewController.h"

@interface CreateCommentViewController ()

@end

@implementation CreateCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = @"Comment";

    self.courseScrollView.frame = CGRectMake (0, 69, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.courseScrollView];
    self.courseScrollView.scrollEnabled = true;
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonPressed)];
    self.navigationItem.rightBarButtonItem = nextButton;
    
    [self setupPickerView];
    [self setupStarRatingView];
    
    self.commentDict = [[NSMutableDictionary alloc] init];
    self.assesmentCommentArray = [[NSMutableArray alloc] init];
    academicYearArray = [[NSMutableArray alloc] initWithObjects:@"2018/19 Sem A",@"2018/19 sem B",@"2017/18 Sem A",@"2017/18 Sem B",@"2016/17 Sem A",@"2016/17 Sem B",@"2015/16 Sem A",@"2015/16 Sem B",@"2014/2015 Sem A",@"2014/2015 Sem B",nil];
    gradeArray = [[NSMutableArray alloc] initWithObjects:@"A",@"A-",@"B+",@"B",@"B-",@"C+",@"C",@"C-",@"D",@"Fail",@"Dropped",nil];
    workLoadArray = [[NSMutableArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",nil];
    assesmentArray = [[NSMutableArray alloc] initWithObjects:@"Group Project",@"Individual Project",@"Exam",@"Assignment",@"In Class Exercise", @"No",nil];
    
    self.courseCodeLabel.layer.cornerRadius = self.courseCodeLabel.frame.size.height/2;
    self.courseCodeLabel.clipsToBounds = YES;
    self.courseCodeLabel.layer.masksToBounds = YES;
    
    self.categoryLabel.layer.cornerRadius = self.categoryLabel.frame.size.height/2;
    self.categoryLabel.clipsToBounds = YES;
    self.categoryLabel.layer.masksToBounds = YES;
}

-(void) viewWillAppear:(BOOL)animated {
    
    self.courseScrollView.contentSize = CGSizeMake(self.scrollViewContentSize.frame.size.width, self.scrollViewContentSize.frame.size.height+100);
    
    
    self.courseCodeLabel.text = [[self.courseInfoDict objectForKey:@"courseCode"] uppercaseString];
    self.categoryLabel.text = [[self.courseInfoDict objectForKey:@"category"] uppercaseString];
    
    [self.commentDict removeAllObjects];
    [self.assesmentCommentArray removeAllObjects];
    [self setupTextField];
    
}

#pragma mark - textField delegaion
-(void) textFieldDidBeginEditing:(UITextField *)textField{
    currentTextField = textField;
    if(textField == self.academicYearTextField){
        currentArray = academicYearArray;
        self.academicYearTextField.inputView = pickerView;
        [pickerView selectRow:0 inComponent:0 animated:true];
    }else if(textField == self.gradeTextField){
        currentArray = gradeArray;
        self.gradeTextField.inputView = pickerView;
        [pickerView selectRow:0 inComponent:0 animated:true];
    }else if(textField == self.workLoadTextField){
        currentArray = workLoadArray;
        self.workLoadTextField.inputView = pickerView;
        [pickerView selectRow:0 inComponent:0 animated:true];
    }else if(textField == self.assesmentItem1TextField){
        currentArray = assesmentArray;
        self.assesmentItem1TextField.inputView = pickerView;
        [pickerView selectRow:0 inComponent:0 animated:true];
    }else if(textField == self.assesmentItem2TextField){
        currentArray = assesmentArray;
        self.assesmentItem2TextField.inputView = pickerView;
        [pickerView selectRow:0 inComponent:0 animated:true];
    }else if(textField == self.assesmentItem3TextField){
        currentArray = assesmentArray;
        self.assesmentItem3TextField.inputView = pickerView;
        [pickerView selectRow:0 inComponent:0 animated:true];
    }
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    
    if(currentTextField == self.academicYearTextField){
        [self.commentDict setObject:self.academicYearTextField.text forKey:@"academicYear"];
    }else if (currentTextField == self.tutorNameTextField){
        [self.commentDict setObject:self.tutorNameTextField.text forKey:@"tutorName"];
    }else if(currentTextField == self.gradeTextField){
        [self.commentDict setObject:self.gradeTextField.text forKey:@"grade"];
    }else if(currentTextField == self.workLoadTextField){
        [self.commentDict setObject:self.workLoadTextField.text forKey:@"workLoad"];
    }else if(currentTextField == self.assesmentItem1TextField){
        [self.assesmentCommentArray addObject:self.assesmentItem1TextField.text];
    }else if(currentTextField == self.assesmentItem2TextField){
        [self.assesmentCommentArray addObject:self.assesmentItem2TextField.text];
    }else if(currentTextField == self.assesmentItem3TextField){
        [self.assesmentCommentArray addObject:self.assesmentItem3TextField.text];
    }
}

#pragma mark - pickerViewDelegate
-(NSInteger)pickerView: (UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [currentArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [currentArray objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    [currentTextField setText:[currentArray objectAtIndex:row]];
    [pickerView reloadAllComponents];
}

#pragma mark -self-defined functions
-(void) doneButtonPressed {
    [currentTextField resignFirstResponder];
}

-(void) showNotFillinAllBlankAlert {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please fill in all the fileds" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) nextButtonPressed {
    
    if([self.gradeTextField.text isEqual:@""]|| [self.workLoadTextField.text isEqual:@""] ||[self.tutorNameTextField.text isEqual:@""]
        || [self.academicYearTextField.text isEqual:@""]||[self.assesmentItem1TextField.text isEqual:@""] || [self.assesmentItem2TextField.text isEqual:@""] || [self.assesmentItem3TextField.text isEqual:@""]){
        [self showNotFillinAllBlankAlert];
    }else{
        [self.commentDict setObject:[self.courseCodeLabel.text lowercaseString] forKey:@"courseCode"];
        [self.commentDict setObject:[self.categoryLabel.text lowercaseString] forKey:@"category"];
        [self.commentDict setObject:self.assesmentCommentArray forKey:@"assesments"];
        
        if(!self.commentTextViewController)
            self.commentTextViewController = [[CommentTextViewController alloc] initWithNibName:@"CommentTextViewController" bundle:nil];
        self.commentTextViewController.delegate = self;
        self.commentTextViewController.uploadCommentDict = self.commentDict;
        self.commentTextViewController.courseID = [self.courseCodeLabel.text lowercaseString];
        [self.navigationController pushViewController:self.commentTextViewController animated:YES];
    }
}

-(void) setupTextField {
    self.academicYearTextField.text = @"";
    self.gradeTextField.text = @"";
    self.suggestRateTextField.text = @"";
    self.workLoadTextField.text = @"";
    self.tutorNameTextField.text = @"";
    self.assesmentItem1TextField.text = @"";
    self.assesmentItem2TextField.text = @"";
    self.assesmentItem3TextField.text =@"";
}

-(void) setupPickerView {
    
    pickerView = [[UIPickerView alloc]init];
    pickerView.delegate = self;
    
    UIToolbar *toolBar = [[UIToolbar alloc] init];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.translucent = true;
    [toolBar sizeToFit];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    [toolBar setItems:[NSArray arrayWithObject:doneButton]];
    
    self.academicYearTextField.inputAccessoryView = toolBar;
    self.gradeTextField.inputAccessoryView = toolBar;
    self.workLoadTextField.inputAccessoryView = toolBar;
    self.assesmentItem1TextField.inputAccessoryView = toolBar;
    self.assesmentItem2TextField.inputAccessoryView = toolBar;
    self.assesmentItem3TextField.inputAccessoryView = toolBar;
}

-(void) setupStarRatingView{
    self.starRating = [[HCSStarRatingView alloc] init];
    self.starRating.value = 0;
    [self.starRating addTarget:self action:@selector(didStarRatingTriggered:) forControlEvents:UIControlEventValueChanged];
}

- (IBAction)didStarRatingTriggered:(HCSStarRatingView *)sender{
    NSString *value = [NSString stringWithFormat:@"%.0f",sender.value];
    [self.commentDict setObject:value forKey:@"suggestRate"];
    
}
@end
