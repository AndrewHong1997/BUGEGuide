//
//  CommentTextViewController.h
//  BUGEGuide
//
//  Created by Andrew Leung on 4/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Firebase.h"
#import "GlobalFunctions.h"

@protocol CommmentTextViewControllerDeleagte;

@interface CommentTextViewController : UIViewController<UITextViewDelegate>

@property (nonatomic, assign)id<CommmentTextViewControllerDeleagte>delegate;
@property (nonatomic, retain) NSMutableDictionary *uploadCommentDict;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UITextView *textView;
@property (retain, nonatomic) FIRDatabaseReference *ref;
@property NSString *courseID;

@end

@protocol CommmentTextViewControllerDeleagte <NSObject>
@end
