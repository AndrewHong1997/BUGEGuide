//
//  CreateCommentViewController.h
//  BUGEGuide
//
//  Created by Andrew Leung on 3/9/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentTextViewController.h"
#import "HCSStarRatingView.h"

@protocol CreateCommentViewContrillerDelegate;

@interface CreateCommentViewController : UIViewController<UITextFieldDelegate,UIPickerViewDelegate,CommmentTextViewControllerDeleagte>{
    
    NSMutableArray *academicYearArray;
    NSMutableArray *gradeArray;
    NSMutableArray *workLoadArray;
    NSMutableArray *suggestRateArray;
    NSString *texFieldName;
    NSMutableArray *currentArray;
    NSMutableArray *assesmentArray;
    UITextField *currentTextField;
    UIPickerView *pickerView;
}

@property (nonatomic, retain) CommentTextViewController *commentTextViewController;
@property (nonatomic, retain) id<CreateCommentViewContrillerDelegate>delegate;
@property (retain, nonatomic) IBOutlet UILabel *courseCodeLabel;
@property (retain, nonatomic) IBOutlet UILabel *categoryLabel;
@property (retain, nonatomic) IBOutlet UITextField *academicYearTextField;
@property (retain, nonatomic) IBOutlet UITextField *tutorNameTextField;
@property (retain, nonatomic) IBOutlet UITextField *gradeTextField;
@property (retain, nonatomic) IBOutlet UITextField *workLoadTextField;
@property (retain, nonatomic) IBOutlet UITextField *suggestRateTextField;
@property (retain, nonatomic) IBOutlet UITextField *assesmentItem1TextField;
@property (retain, nonatomic) IBOutlet UITextField *assesmentItem2TextField;
@property (retain, nonatomic) IBOutlet UITextField *assesmentItem3TextField;
@property (retain, nonatomic) IBOutlet UIScrollView *courseScrollView;
@property (retain, nonatomic) NSDictionary *courseInfoDict;
@property (retain, nonatomic) NSMutableDictionary *commentDict;
@property (retain, nonatomic) NSMutableArray *assesmentCommentArray;
@property (retain, nonatomic) IBOutlet UIView *scrollViewContentSize;
@property (retain, nonatomic) IBOutlet HCSStarRatingView *starRating;

@end

@protocol CreateCommentViewContrillerDelegate<NSObject>

@end

