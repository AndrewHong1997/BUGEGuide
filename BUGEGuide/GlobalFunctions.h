//
//  GlobalFunctions.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 9/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface GlobalFunctions : NSObject

+ (void) addOverlayToView: (UIView *)view;
+ (void) removeOverlay;
+ (void) upperCaseString:(NSString *)string;

@end


