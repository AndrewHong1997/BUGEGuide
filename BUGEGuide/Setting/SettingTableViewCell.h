//
//  SettingTableViewCell.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 8/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *userDataLabel;

@end

NS_ASSUME_NONNULL_END
