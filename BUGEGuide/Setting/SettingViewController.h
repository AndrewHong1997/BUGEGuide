//
//  SettingViewController.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 7/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingTableViewCell.h"

//@protocol SettingViewControllerDelegate;

@interface SettingViewController :UIViewController<UITableViewDelegate,UITableViewDataSource>{
    
    NSArray *cellTitleArray;
}

//@property (nonatomic,retain) id<SettingViewControllerDelegate>delegate;

@property (retain, nonatomic) IBOutlet UIImageView *userImageView;

@property (retain, nonatomic) IBOutlet UITableView *settingTableView;

@property (weak, nonatomic) IBOutlet UIButton *signOutButton;

@property (retain,nonatomic) IBOutlet SettingTableViewCell *ACell;
@end

//@protocol SettingViewControllerDelegate<NSObject>
//@end

