//
//  SettingViewController.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 7/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "SettingViewController.h"
#import "Firebase.h"
#import "AppDelegate.h"
@import GoogleSignIn;

@interface SettingViewController ()

@end

@implementation SettingViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.signOutButton.layer.cornerRadius = self.signOutButton.frame.size.height/2;
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width/2;
    self.userImageView.clipsToBounds = YES;
    
    cellTitleArray = [[NSArray alloc] initWithObjects:@"Name",@"Email",@"Phone Number",@"User ID", nil];
}

-(void) viewWillAppear:(BOOL)animated{
    
    FIRUser *currentUser = [FIRAuth auth].currentUser;
    
    NSString *urlString = [NSString stringWithFormat:@"%@",currentUser.photoURL];
    self.userImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingTableViewCell"];
    if(!cell){
        [[NSBundle mainBundle] loadNibNamed:@"SettingTableViewCell" owner:self options:nil];
        cell = _ACell;
        _ACell = nil;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.titleLabel.text = [cellTitleArray objectAtIndex:indexPath.row];
    
    if(cellTitleArray){
        switch (indexPath.row){
                
            case 0: cell.userDataLabel.text = [FIRAuth auth].currentUser.displayName;
                break;
            case 1: cell.userDataLabel.text = [FIRAuth auth].currentUser.email;
                break;
            case 2: cell.userDataLabel.text = [FIRAuth auth].currentUser.phoneNumber;
                break;
            case 3: cell.userDataLabel.text = [FIRAuth auth].currentUser.uid;
                break;
                default:
                break;
        }
    }
    
    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [cellTitleArray count];
    
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (IBAction)signOutPressed:(id)sender {
    
    UIAlertController *alertOfSignOut = [UIAlertController alertControllerWithTitle:@"Log Out Alert" message:@"Are you sure to Log Out?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[GIDSignIn sharedInstance] signOut];
        AppDelegate *appDelegate= (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate signOut];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertOfSignOut addAction:okAction];
    [alertOfSignOut addAction:cancelAction];
    [self presentViewController:alertOfSignOut animated:YES completion:nil];
    
}



@end
