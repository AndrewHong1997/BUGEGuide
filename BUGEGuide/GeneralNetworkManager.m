//
//  GeneralNetworkManager.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 10/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "GeneralNetworkManager.h"

@implementation GeneralNetworkManager

+ (GeneralNetworkManager *)manager {
    
    static dispatch_once_t onceToken;
    static GeneralNetworkManager *instance = nil;
    dispatch_once(&onceToken, ^{
      
        instance = [[GeneralNetworkManager alloc] init];
        
    });
    return instance;
}

- (id) init{
    
    self = [super init];
    if(self){
        self.hostReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
        self.ref = [FIRDatabase database].reference;
    }
    
    return self;
}

-(void) getAllCourseWithCompletionHander:(void (^)(NSDictionary *courseDict, NSError *error))completionHandler{
    
    NetworkStatus networkStatus = [self.hostReachability currentReachabilityStatus];
    
    if(networkStatus == NotReachable){
        
        NSError *nonetworkError = [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
        completionHandler(nil,nonetworkError);
    }
    
    FIRUser *currentUser = [FIRAuth auth].currentUser;
    
    [currentUser getIDTokenForcingRefresh:NO completion:^(NSString *idToken, NSError *error) {
        
        NSString *urlString = [NSString stringWithFormat:@"https://bugeguide.firebaseio.com/courseInfo.json/?auth=%@",idToken];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL
                                                                            URLWithString:urlString]cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                           timeoutInterval:10
                                        ];
        [request setHTTPMethod:@"GET"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error){
                
                completionHandler(nil,error);
                
            }
                 NSMutableDictionary *courseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
            completionHandler(courseDict,nil);
            
        }]resume];
    }];
}


-(void) getCommentsWithCompletionHandler:(void (^)(NSDictionary *, NSError *))completionHandler{
    
    NetworkStatus networkStatus = [self.hostReachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        NSError *networkError = [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
        completionHandler(nil,networkError);
    }
    FIRUser *currentUser = [FIRAuth auth].currentUser;
    [currentUser getIDTokenForcingRefresh:NO completion:^(NSString *idToken, NSError *error) {
        NSString *urlString = [NSString stringWithFormat:@"https://bugeguide.firebaseio.com/user-posts.json/?auth=%@",idToken];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10
                                        ];
        [request setHTTPMethod:@"GET"];
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse *response, NSError *error) {
            
            if(error){
                completionHandler(nil,error);
            }
            NSMutableDictionary *commentDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            completionHandler(commentDict,nil);
            
        }]resume];
    }];
    
}


@end
