//
//  AppDelegate.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 6/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "AppDelegate.h"
#import "Firebase.h"
#import "GlobalFunctions.h"
@import GoogleSignIn;
@import FirebaseAuth;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [FIRApp configure];
    
    [GIDSignIn sharedInstance].clientID = [FIRApp defaultApp].options.clientID;
    [GIDSignIn sharedInstance].delegate = self;
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-4412251054287385~2931288742"];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
   
    FIRUser *currentUser = [FIRAuth auth].currentUser;

    if(currentUser){

        self.tabBarViewController = [[MenuTabBarViewController alloc] init];
        [self.window setRootViewController:self.tabBarViewController];
        [self.window makeKeyAndVisible];
    }else {
        self.mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
        self.mainViewController.view.frame = self.window.bounds;
        self.window.rootViewController = self.mainViewController;
        [self.window makeKeyAndVisible];
    }
    
    self.hostReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    return YES;
    
}


-(BOOL) application:(UIApplication *)application openURL:(nonnull NSURL *)url options:(nonnull NSDictionary<NSString *,id> *)options{
    
    return [[GIDSignIn sharedInstance] handleURL:url sourceApplication:options [UIApplicationOpenURLOptionsAnnotationKey]annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - GIDSignIn Delegate

-(void) signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error{
    
    NetworkStatus networkStatus = [self.hostReachability currentReachabilityStatus];
    
    if(networkStatus == NotReachable){
        
        NSError *nonetworkError = [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
        NSString *errorCode = [NSString stringWithFormat:@"%@",nonetworkError];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:errorCode message:@"Your current network got some error,please try it later" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self.mainViewController presentViewController:alert animated:YES completion:nil];
    }
    if(error == nil){
        
        [GlobalFunctions addOverlayToView:self.mainViewController.view];
        GIDAuthentication *authentication = user.authentication;
        FIRAuthCredential *credential = [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken accessToken:authentication.accessToken];
        
        if([user.hostedDomain isEqualToString:@"life.hkbu.edu.hk"]){
            
            [[FIRAuth auth] signInWithCredential:credential completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
                
                if(error){
                    
                    return;
                }
                
                FIRUser *currentUser = [FIRAuth auth].currentUser;
                
                [currentUser getIDTokenWithCompletion:^(NSString * _Nullable token, NSError * _Nullable error) {
                    
                    if(error){
                        
                        return;
                    }
                    
                    [GlobalFunctions removeOverlay];
                    [self presentTabBarViewController];
                }];
                
            }];
        }
    }
}

#pragma mark - self defined function

-(BOOL) validateRegistryEmail :(NSString *)email {
    
    NSString *emailDoamin = @"@life.hkbu.edu.hk";
    NSPredicate *predicateEmail = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailDoamin];
    return [predicateEmail evaluateWithObject:emailDoamin];
}

-(void) presentTabBarViewController {
    
    self.tabBarViewController = [[MenuTabBarViewController alloc] init];
    [self.window setRootViewController:self.tabBarViewController];
    [self.tabBarViewController setSelectedIndex:0];
    self.mainViewController = nil;


}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"BUGEGuide"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}


#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

-(void) signOut {
    
    if(!self.mainViewController)
        self.mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    self.mainViewController.view.frame = self.window.bounds;
    self.window.rootViewController = self.mainViewController;
}

@end
