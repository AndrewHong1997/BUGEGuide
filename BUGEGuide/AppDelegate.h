//
//  AppDelegate.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 6/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MainViewController.h"
#import "MenuTabBarViewController.h"
#import "HomeViewController.h"
#import "SettingViewController.h"
#import "Reachability.h"


@import GoogleSignIn;

@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MainViewController *mainViewController;
@property (nonatomic,retain)  MenuTabBarViewController *tabBarViewController;
@property (nonatomic,retain) UINavigationController *navigationController;
@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (retain, nonatomic) Reachability *hostReachability;

- (void)saveContext;
-(void) signOut;

@end

