//
//  CourseDetailTableViewCell.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 9/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "CourseDetailTableViewCell.h"

@implementation CourseDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.courseImageView.layer.cornerRadius = self.courseImageView.layer.frame.size.height/2;
    self.courseImageView.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
