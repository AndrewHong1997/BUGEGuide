//
//  MenuTabBarViewController.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 7/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "SearchTableViewController.h"
#import "SettingViewController.h"
#import "Firebase.h"
#import "GlobalFunctions.h"
#import "CourseDetailViewController.h"


@interface MenuTabBarViewController : UITabBarController<SearchTableViewControllerDelegate,UINavigationControllerDelegate,UISplitViewControllerDelegate,CourseDetailViewControllerDelegate>

@property (nonatomic, retain) CourseDetailViewController *courseDetailViewController;
@property (nonatomic, retain) UISplitViewController *splitViewController;
@property (nonatomic, retain) HomeViewController *homeViewController;
@property (nonatomic, retain) SearchTableViewController *searchTableViewController;
@property (nonatomic, retain) SettingViewController *settingViewController;
@property (nonatomic, retain) FIRDatabaseReference *ref;



@end


