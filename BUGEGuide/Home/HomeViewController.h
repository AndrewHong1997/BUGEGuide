//
//  HomeViewController.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 7/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseDetailViewController.h"
#import "HomeTableViewCell.h"
#import "Reachability.h"


@protocol HomeViewControllerDelegate;


@interface HomeViewController :UIViewController<UINavigationBarDelegate,CourseDetailViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>{
    
    NSArray *cellStandardTitleArray;
    BOOL searchEnabled;
}


@property (retain, nonatomic) CourseDetailViewController *courseDetailViewController;
@property (assign, nonatomic) id<HomeViewControllerDelegate>delegate;
@property (retain, nonatomic) IBOutlet UITableView *courseTableView;
@property (retain, nonatomic) IBOutlet HomeTableViewCell *ACell;
@property (retain, nonatomic) NSArray *cellTitleArray;
@property (retain, nonatomic) FIRDatabaseReference *ref;

@property (retain, nonatomic) Reachability *hostReachability;

@end

@protocol HomeViewControllerDelegate<NSObject>
@end;


