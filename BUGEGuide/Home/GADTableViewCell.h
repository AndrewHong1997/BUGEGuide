//
//  GADTableViewCell.h
//  BUGEGuide
//
//  Created by Andrew Leung on 10/22/18.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GADTableViewCell : UITableViewCell
//@property (retain, nonatomic) IBOutlet GADMediaView *mediaImageView;
@property (retain, nonatomic) IBOutlet UILabel *headLineView;
@property (retain, nonatomic) IBOutlet UILabel *priceView;
@property (retain, nonatomic) IBOutlet UILabel *starRatingView;
@property (retain, nonatomic) IBOutlet UIButton *callToActionButton;
@property (retain, nonatomic) IBOutlet UILabel *bodyView;

@end

