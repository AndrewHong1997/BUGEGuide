//
//  MenuTabBarViewController.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 7/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "MenuTabBarViewController.h"

@interface MenuTabBarViewController ()

@end

@implementation MenuTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    if(!self.splitViewController)
        self.splitViewController = [[UISplitViewController alloc] init];
    self.splitViewController.delegate = self;

    if(!self.homeViewController)
        self.homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    UINavigationController *nav1 = [[UINavigationController alloc]initWithRootViewController:self.homeViewController];
    nav1.navigationBar.topItem.title = @"Home";


    if(!self.searchTableViewController)
        self.searchTableViewController = [[SearchTableViewController alloc] initWithNibName:@"SearchTableViewController" bundle:nil];
    self.searchTableViewController.delegate = self;
    UINavigationController *nav2 = [[UINavigationController alloc] initWithRootViewController:self.searchTableViewController];
    nav2.navigationBar.topItem.title = @"Search";


    if(!self.settingViewController)
        self.settingViewController = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
    UINavigationController *nav3 = [[UINavigationController alloc] initWithRootViewController:self.settingViewController];
    nav3.navigationBar.topItem.title = @"Setting";
    
    if(!self.courseDetailViewController)
        self.courseDetailViewController = [[CourseDetailViewController alloc] initWithNibName:@"CourseDetailViewController" bundle:nil];
    UINavigationController *detailViewNavigation = [[UINavigationController alloc] initWithRootViewController:self.courseDetailViewController];
    
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
    self.splitViewController.viewControllers = @[nav1,detailViewNavigation];

    [self setViewControllers:[NSArray arrayWithObjects:self.splitViewController,nav2,nav3,nil]];
    [self setSelectedIndex:0];

    self.splitViewController.tabBarItem.image = [UIImage imageNamed:@"home.png"];
    self.splitViewController.tabBarItem.selectedImage = [UIImage imageNamed:@"home_active.png"];
    self.splitViewController.tabBarItem.title = @"Home";
    
    nav2.tabBarItem.image = [UIImage imageNamed:@"library.png"];
    nav2.tabBarItem.selectedImage = [UIImage imageNamed:@"library_active.png"];
    nav2.tabBarItem.title = @"Search";
    
    nav3.tabBarItem.image = [UIImage imageNamed:@"setting.png"];
    nav3.tabBarItem.selectedImage = [UIImage imageNamed:@"setting_active.png"];
    nav3.tabBarItem.title = @"Settings";
    
    self.tabBar.layer.opacity = 0.96f;
    self.tabBar.backgroundColor = UIColor.clearColor;
    UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView * blurEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    [blurEffectView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    blurEffectView.frame = self.tabBar.bounds;
    [self.tabBar addSubview:blurEffectView];
    [self.tabBar sendSubviewToBack:blurEffectView];
    [self setSelectedIndex:0];
    self.ref = [[FIRDatabase database] reference];
    
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tabBar.layer.opacity = 0.96f;
    self.tabBar.backgroundColor = UIColor.clearColor;
    
    UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView * blurEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    blurEffectView.frame = self.tabBar.bounds;
    [self.tabBar addSubview:blurEffectView];
    [self.tabBar sendSubviewToBack:blurEffectView];
}

-(void) tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if(tabBar.items[1] == item){
        [self prepareForData];
    }
}

-(void) prepareForData{

    self.searchTableViewController.courseIDArray = [[NSArray alloc] init];
    NSMutableArray *courseIDArray = [[NSMutableArray alloc] init];
    [GlobalFunctions addOverlayToView:self.view];
    [[GeneralNetworkManager manager] getAllCourseWithCompletionHander:^(NSDictionary *courseDict, NSError *error) {
        if(error == nil){
            
        NSMutableDictionary *courseInfoDict = [[NSMutableDictionary alloc] initWithDictionary:courseDict];
        for(NSString *key in [courseInfoDict allKeys]){
            [courseIDArray addObject:key];
        }
        self.searchTableViewController.courseIDArray = courseIDArray;
        self.searchTableViewController.courseDict = courseInfoDict;
        dispatch_async(dispatch_get_main_queue(),^{
            [GlobalFunctions removeOverlay];
            [self.searchTableViewController.searchTabelView reloadData];
        });
      }
    }];
    
}

- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    
    return true;
}

@end
