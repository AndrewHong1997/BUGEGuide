//
//  CourseDetailInfoViewController.h
//  BUGEGuide
//
//  Created by Andrew Leung on 25/8/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseDetailInfoTableViewCell.h"
#import "CreateCommentViewController.h"
#import "CommentTableViewCell.h"
#import "GADTableViewCell.h"
#import "GeneralNetworkManager.h"
#import "Reachability.h"
#import "Firebase.h"
@import GoogleMobileAds;

@protocol CourseDetailInfoViewControllerDelegate;

@interface CourseDetailInfoViewController : UIViewController<UINavigationBarDelegate,UITableViewDelegate,UITableViewDataSource,CreateCommentViewContrillerDelegate,GADBannerViewDelegate>{
    
    NSArray *cellStandardTitleArray;
    NSArray *offeringDepartmentArray;
    NSString *offerDepartment;
    NSString *coreStatus;
    NSString *igeStatus;
    UIRefreshControl *refreshView;
    NSUInteger adInterval;
    
}


@property (retain, nonatomic) CreateCommentViewController *createCommentViewController;

@property (assign, nonatomic) id<CourseDetailInfoViewControllerDelegate>delegate;
@property (retain, nonatomic) IBOutlet UITableView *courseDetailInfoTableView;
@property (retain, nonatomic) IBOutlet CourseDetailInfoTableViewCell *Acell;
@property (retain, nonatomic) IBOutlet CommentTableViewCell *BCell;
@property (retain, nonatomic) IBOutlet GADTableViewCell *Ccell;

@property (retain, nonatomic) NSArray *cellTitleArray;
@property (retain, nonatomic) NSDictionary *courseInfoDict;
@property (retain, nonatomic) NSMutableDictionary *departmentDict;
@property (retain, nonatomic) NSArray *commentIDArray;
@property (retain, nonatomic) NSMutableDictionary *commentDict;
@property (retain, nonatomic) NSMutableDictionary *tempDict;

@property (retain, nonatomic) IBOutlet UISegmentedControl *segementedControl;
@property (retain, nonatomic) Reachability *hostReachability;
@property (retain, nonatomic) FIRDatabaseReference *ref;

@property (retain, nonatomic) GADBannerView *bannerView;


@end

@protocol CourseDetailInfoViewControllerDelegate<NSObject>
@end


