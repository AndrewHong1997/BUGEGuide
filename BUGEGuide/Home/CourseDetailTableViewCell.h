//
//  CourseDetailTableViewCell.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 9/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CourseDetailTableViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *courseCodeLabel;
@property (retain, nonatomic) IBOutlet UILabel *courseTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *areaTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *areaLabel;
@property (retain, nonatomic) IBOutlet UILabel *commentTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *commentLabel;
@property (retain, nonatomic) IBOutlet UIImageView *courseImageView;


@end

