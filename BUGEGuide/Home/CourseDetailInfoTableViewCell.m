//
//  CourseDetailInfoTableViewCell.m
//  BUGEGuide
//
//  Created by Andrew Leung on 25/8/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "CourseDetailInfoTableViewCell.h"

@implementation CourseDetailInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
