//
//  HomeTableViewCell.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 9/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HomeTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *categoryLabel;

@end

