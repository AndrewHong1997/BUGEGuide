//
//  CourseDetailViewController.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 8/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "CourseDetailViewController.h"
#import "GlobalFunctions.h"

@interface CourseDetailViewController ()

@end

@implementation CourseDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ref = [FIRDatabase database].reference;
    self.hostReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    if(self.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable){
        [self registerForPreviewingWithDelegate:self sourceView:self.view];
    }
    
    self.courseDetailView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 0);
}

-(void) viewWillAppear:(BOOL)animated{
    
    [self reloadAllObject];
    [self getCourseInfo];
}

-(void) getCourseInfo {
    
    NSMutableArray *courseIDArray = [[NSMutableArray alloc] init];
    [courseIDArray removeAllObjects];
    FIRDatabaseQuery *query = [[[self.ref child:@"courseInfo"]queryOrderedByChild:@"category"] queryEqualToValue:self.title];
    [query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {
        
        if(snapshot.value == [NSNull null]){
            dispatch_async(dispatch_get_main_queue(),^{
                
            });
        }else{
            
            self.courseDict = [[NSMutableDictionary alloc] initWithDictionary:snapshot.value];
            for(NSString *key in [self.courseDict allKeys]){
                [courseIDArray addObject:key];
            }
            
            self.courseArray = [[NSMutableArray alloc] initWithArray:courseIDArray];
                dispatch_async(dispatch_get_main_queue(),^{
                    [self.courseDetailView reloadData];
                });
        }

    }];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CourseDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CourseDetailTableViewCell"];
   
    if(!cell){
        [[NSBundle mainBundle] loadNibNamed:@"CourseDetailTableViewCell" owner:self options:nil];
        cell = _Acell;
        _Acell = nil;
    }
    
        NSString *courseID = [self.courseArray objectAtIndex:indexPath.row];
        NSDictionary *cellDict = [self.courseDict objectForKey:courseID];
    
        NSString *courseCode = [[cellDict objectForKey:@"courseCode"] uppercaseString];

        cell.courseCodeLabel.text  =[self removeCharToString: courseCode];
        cell.courseTitleLabel.text = [cellDict objectForKey:@"title"];
        cell.areaLabel.text = [cellDict objectForKey:@"remarks"];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    
    [self addCourseImage:cell.courseImageView];
    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 180;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.courseArray count];
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(!self.courseDetailInfoViewController)
        self.courseDetailInfoViewController = [[CourseDetailInfoViewController alloc] initWithNibName:@"CourseDetailInfoViewController" bundle:nil];
    
         NSArray *courseArray = [self.courseDict allKeys];
         NSString *courseID = [courseArray objectAtIndex:indexPath.row];
    
         NSDictionary *cellDict = [self.courseDict objectForKey:courseID];
    
         self.courseDetailInfoViewController.delegate = self;
         self.courseDetailInfoViewController.courseInfoDict = cellDict;
          NSString *courseCode = [[cellDict objectForKey:@"courseCode"] uppercaseString];

         self.courseDetailInfoViewController.title = [self removeCharToString: courseCode];
    
    if(![self.navigationController.topViewController isKindOfClass:[self.courseDetailInfoViewController class]]){
        [self.navigationController pushViewController:self.courseDetailInfoViewController animated:YES];
    }
}

#pragma mark -3D touch delegation method

-(UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location{
    
    location = [self.courseDetailView convertPoint:location fromView:[previewingContext sourceView]];
    NSIndexPath *path = [self.courseDetailView indexPathForRowAtPoint:location];
    
    if(path){
        CourseDetailInfoViewController *courseDetailInfoViewController = [[CourseDetailInfoViewController alloc] initWithNibName:@"CourseDetailInfoViewController" bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:courseDetailInfoViewController];
        
        NSArray *courseArray = [self.courseDict allKeys];
        NSString *courseID = [courseArray objectAtIndex:path.row];
        NSDictionary *cellDict = [self.courseDict objectForKey:courseID];

        courseDetailInfoViewController.delegate = self;
        courseDetailInfoViewController.courseInfoDict = cellDict;
        
        NSString *courseCode = [[cellDict objectForKey:@"courseCode"] uppercaseString];
        
        courseDetailInfoViewController.title = [self removeCharToString: courseCode];
        
        return nav;
    }
    return nil;
}

-(void) previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit{
    
    [self showViewController:viewControllerToCommit sender:self];
}


-(void) addCourseImage :(UIImageView *)imageView {
    if([self.title isEqualToString:@"languages"]){
        [imageView setImage:[UIImage imageNamed:@"language.png"]];
    }else if([self.title isEqualToString:@"information management technology"]){
        [imageView setImage: [UIImage imageNamed:@"it.png"]];
    }else if([self.title isEqualToString:@"numeracy"]){
        [imageView setImage:[UIImage imageNamed:@"numeracy.png"]];
    }else if([self.title isEqualToString:@"physical education"]){
        [imageView setImage:[UIImage imageNamed:@"pe.png"]];
    }else if([self.title isEqualToString:@"history and civilisation"]){
        [imageView setImage:[UIImage imageNamed:@"hc.png"]];
    }else if([self.title isEqualToString:@"values and the meaning of life"]){
        [imageView setImage:[UIImage imageNamed:@"vm.png"]];
    }else if([self.title isEqualToString:@"arts"]){
        [imageView setImage:[UIImage imageNamed:@"arts.png"]];
    }else if([self.title isEqualToString:@"business"]){
        [imageView setImage:[UIImage imageNamed:@"business.png"]];
    }else if([self.title isEqualToString:@"communication/visual arts"]){
        [imageView setImage:[UIImage imageNamed:@"communication.png"]];
    }else if([self.title isEqualToString:@"science/chinese medicine"]){
        [imageView setImage:[UIImage imageNamed:@"science.png"]];
    }else {
        [imageView setHidden:YES];
    }
}

-(void) reloadAllObject {
    [self.courseArray removeAllObjects];
    [self.courseDetailView reloadData];
}

-(NSString *) removeCharToString:(NSString*) courseCode{
    NSCharacterSet *removeChar = [NSCharacterSet characterSetWithCharactersInString:@"_"];
    courseCode = [[courseCode componentsSeparatedByCharactersInSet:removeChar] componentsJoinedByString:@"/"];
    return courseCode;
}
@end
