//
//  HomeViewController.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 7/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "HomeViewController.h"
#import "GeneralNetworkManager.h"
#import "GlobalFunctions.h"


@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  cellStandardTitleArray = [[NSArray alloc] initWithObjects:@"languages",@"information management technology",@"numeracy",@"physical education",@"history and civilisation",@"values and the meaning of life",@"arts",@"business",@"communication/visual arts",@"science/chinese medicine",@"social sciences",@"interdisciplinary",@"history and civilisation",@"values and the meaning of life",@"quantitative reasoning",nil];
    self.cellTitleArray = cellStandardTitleArray;
    self.hostReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
}

-(void) viewWillAppear:(BOOL)animated{
    [self.courseTableView reloadData];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeTableViewCell"];
    
    int row = 0;
    for (int i = 0; i<indexPath.section; i++)
        row += [tableView numberOfRowsInSection:i];
    row += indexPath.row;
    
    if(!cell){
        if(self.cellTitleArray == cellStandardTitleArray){
            [[NSBundle mainBundle] loadNibNamed:@"HomeTableViewCell" owner:self options:nil];
            cell = _ACell;
            _ACell = nil;
        }
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.categoryLabel.text = [self.cellTitleArray objectAtIndex:row];
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if(self.cellTitleArray == cellStandardTitleArray){
        
        NSArray *sectionNames = [[NSArray alloc] initWithObjects:@"Core Requirements",@"Distribution Requirements", @"New Year 1 intake",nil];
        return sectionNames[section];
    }
    return 0;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(self.cellTitleArray == cellStandardTitleArray){
        if(section == 0)
            return 6;
        else
            if(section ==1)
                return 6;
            else return 3;
    }else{
         return [self.cellTitleArray count];
    }
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    
    if(self.cellTitleArray == cellStandardTitleArray)
        return 3;
    else
        return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0f;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int row = 0;
    for (int i = 0; i<indexPath.section; i++)
        row += [tableView numberOfRowsInSection:i];
    row += indexPath.row;
    
    if(self.cellTitleArray == self->cellStandardTitleArray)
        if(!self.courseDetailViewController)
            self.courseDetailViewController = [[CourseDetailViewController alloc] initWithNibName:@"CourseDetailViewController" bundle:nil];
    self.courseDetailViewController.delegate = self;
    self.courseDetailViewController.title = [self.cellTitleArray objectAtIndex:row];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.courseDetailViewController];
    [self showDetailViewController:nav sender:self];

}

-(void) showConnectionErrorAlert {
    NSError *nonetworkError = [[NSError alloc] initWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
    NSString *errorCode = [NSString stringWithFormat:@"%@",nonetworkError];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:errorCode message:@"Your current network got some error,please try it later" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:alertAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
