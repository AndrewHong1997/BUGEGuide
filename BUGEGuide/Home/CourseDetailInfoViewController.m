//
//  CourseDetailInfoViewController.m
//  BUGEGuide
//
//  Created by Andrew Leung on 25/8/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "CourseDetailInfoViewController.h"

@interface CourseDetailInfoViewController ()

@end

@implementation CourseDetailInfoViewController


- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupRefreshControl];
    
    cellStandardTitleArray = [[NSArray alloc] initWithObjects:@"Course Title",@"Couse Code",@"Offering Department",@"Core Requirement",@"IGE Status",@"Unit,Lecture Hours,Tutorial Time",@"Course URL", nil];
    UIBarButtonItem *commentButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"edit.png"] landscapeImagePhone:nil style:UIBarButtonItemStylePlain target:self action:@selector(commentButtonPressed)];
    self.navigationItem.rightBarButtonItem = commentButton;
    
    [self.segementedControl addTarget:self action:@selector(indexDidChangeForSegmentedControl:) forControlEvents:UIControlEventValueChanged];
    self.ref = [FIRDatabase database].reference;
    self.hostReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    self.bannerView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
    self.bannerView.adUnitID = @"ca-app-pub-4412251054287385/6357621761";
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:[GADRequest request]];
    self.bannerView.delegate = self;

}

-(void) viewWillAppear:(BOOL)animated{
    
    [self.courseDetailInfoTableView reloadData];
    [self getCommentData];
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    int row = 0;
    for (int i = 0; i<indexPath.section; i++)
        row += [tableView numberOfRowsInSection:i];
    row += indexPath.row;
    
    if(tableView.tag == 0){
        CourseDetailInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CourseDetailInfoTableViewCell"];
        if(!cell){
            [[NSBundle mainBundle] loadNibNamed:@"CourseDetailInfoTableViewCell" owner:self options:nil];
            cell = _Acell;
            _Acell = nil;
        }
        
       cell.titleLable.text = [cellStandardTitleArray objectAtIndex:indexPath.row];
    
        NSString *courseTitle = [self.courseInfoDict objectForKey:@"title"];
        NSString *lowerCaseCourseCode = [self.courseInfoDict objectForKey:@"courseCode"];
        NSString *upperCaseCourseCode = [lowerCaseCourseCode uppercaseString];
        NSString *unitHours = [self.courseInfoDict objectForKey:@"unitHours"];
        NSString *courseInfoUrl = [self.courseInfoDict objectForKey:@"courseInfoUrl"];
        
        NSCharacterSet *removeChar = [NSCharacterSet characterSetWithCharactersInString:@"_"];
        NSString *resultCourseCode = [[upperCaseCourseCode componentsSeparatedByCharactersInSet:removeChar] componentsJoinedByString:@"/"];
        
        bool coreRequirementStatus = [[self.courseInfoDict objectForKey:@"isCoreRequirement"] boolValue];
        if(coreRequirementStatus == true){
            coreStatus = @"YES";
        }else{
            coreStatus = @"NO";
        }
        bool ige = [[self.courseInfoDict objectForKey:@"isIGE"] boolValue];
        if(ige == true){
            igeStatus = @"YES";
            offeringDepartmentArray = [self.courseInfoDict objectForKey:@"offeringDepartment"];
            for(int i=0; i<[offeringDepartmentArray count]; i++){
                offerDepartment = [[[offeringDepartmentArray objectAtIndex:i] allKeys] firstObject];
            }
        }else{
            igeStatus = @"NO";
            offerDepartment = [[self.courseInfoDict objectForKey:@"offeringDepartment"] firstObject];
        }
        
        NSMutableArray *cellArray = [[NSMutableArray alloc] initWithObjects:courseTitle,resultCourseCode,offerDepartment,coreStatus,igeStatus,unitHours,courseInfoUrl, nil];
        
        if(cellStandardTitleArray){
            [cell.infoLabel setText:[cellArray objectAtIndex:indexPath.row]];
            return cell;
        }
    }else if(tableView.tag == 1){
        CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentTableViewCell"];
        if(!cell){
            [[NSBundle mainBundle] loadNibNamed:@"CommentTableViewCell" owner:self options:nil];
            cell = _BCell;
            _BCell = nil;
        }

        NSArray *commentArray = [self.tempDict allKeys];
        NSString *commentID = [commentArray objectAtIndex:row];
        NSDictionary *cellDict = [self.tempDict objectForKey:commentID];
        
        double timeStamp = [[cellDict objectForKey:@"createTime"] doubleValue];
        NSString *dateString = [self timeStampToDateString:timeStamp];
        
        double suggestRate = [[cellDict objectForKey:@"suggestRate"] doubleValue];

        cell.academicYearLabel.text = [cellDict objectForKey:@"academicYear"];
        cell.gradeLabel.text = [cellDict objectForKey:@"grade"];
        cell.commentTextView.text = [cellDict objectForKey:@"commentContent"];
        cell.tutorNameLabel.text = [cellDict objectForKey:@"tutorName"];
        cell.workLoadLabel.text = [cellDict objectForKey:@"workLoad"];
        cell.timeStampLabel.text = dateString;
        [cell.starRatingView setValue: suggestRate];
         
        return cell;
    }
    return nil;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag ==0){
        return 100;
    }else if(tableView.tag ==1){
        return 250;
    }
    return 0;
}


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    if(tableView.tag ==0){
        return 1;
    }else if(tableView.tag == 1){
        return [self.commentIDArray count];
    }
    return 0;
}


- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag == 0){
        return [cellStandardTitleArray count];
    }else if(tableView.tag == 1){
        return 1;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView;
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 10)];
    
    return headerView;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 10.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.0f;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,20)];
    [footerView setBackgroundColor: UIColor.clearColor];
    return footerView;
}

-(void) showErrorAlertController {

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Network Error" message:@"Your current network is not reachable" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UISegementedControl delegate

-(void) indexDidChangeForSegmentedControl:(UISegmentedControl *)sender{
    NSInteger selectedIndex = sender.selectedSegmentIndex;
    switch (selectedIndex) {
        case 0:
            sender.selectedSegmentIndex=0;
            self.courseDetailInfoTableView.tag=0;
            [self.courseDetailInfoTableView reloadData];
            break;
        case 1:
            sender.selectedSegmentIndex=1;
            self.courseDetailInfoTableView.tag=1;
            [self.courseDetailInfoTableView reloadData];
            break;
        default:
            break;
    }
}

#pragma mark - self defined function

-(void) setupRefreshControl {
    refreshView = [[UIRefreshControl alloc] init];
    refreshView.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull Down Refresh"];
    [refreshView addTarget:self action:@selector(getCommentData) forControlEvents:UIControlEventValueChanged];
    [self.courseDetailInfoTableView addSubview:refreshView];
    [self.courseDetailInfoTableView sendSubviewToBack:refreshView];
}

-(void) commentButtonPressed {
    
    if(!self.createCommentViewController)
        self.createCommentViewController = [[CreateCommentViewController alloc] initWithNibName:@"CreateCommentViewController" bundle:nil];
    self.createCommentViewController.delegate = self;
    self.createCommentViewController.courseInfoDict = self.courseInfoDict;
    
    if(![self.navigationController.topViewController isKindOfClass:[self.createCommentViewController class]]){
        [self.navigationController pushViewController:self.createCommentViewController animated:YES];
    }
}

-(void)noCommentAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Comments" message:@"No Comments here, try to comment!!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.courseDetailInfoTableView reloadData];
        [self->refreshView endRefreshing];
    }];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)noNetworkAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"NetworkError" message:@"Your current network is not Reachable. Please try it later" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.courseDetailInfoTableView reloadData];
        [self->refreshView endRefreshing];
    }];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

- (NSString *)timeStampToDateString: (double) timeStamp{
    NSTimeInterval timeInterval=timeStamp/1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyy-MM-dd HH:mm:SS"];
    NSString *dateString=[dateformatter stringFromDate:date];
    return dateString;
}


-(void) getCommentData {
    NetworkStatus netStatus = [self.hostReachability currentReachabilityStatus];
    if(netStatus == NotReachable){
        [self noNetworkAlert];
    }
    
    NSString *courseCode = [self.title lowercaseString];
    
    self.commentIDArray = [[NSArray alloc] init];
    FIRDatabaseQuery *query = [[[self.ref child:@"user-posts"] queryOrderedByKey] queryEqualToValue:courseCode];
    
    [query observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot.value == [NSNull null]){
            dispatch_async(dispatch_get_main_queue(),^{
                [self.courseDetailInfoTableView reloadData];
                [self->refreshView endRefreshing];
            });
            
        }else {
            self.commentDict = [[NSMutableDictionary alloc] initWithDictionary:snapshot.value];
            self.commentIDArray = [self.commentDict objectForKey:courseCode];
            self.tempDict = [self.commentDict objectForKey:courseCode];
            dispatch_async(dispatch_get_main_queue(),^{
                [self.courseDetailInfoTableView reloadData];
                [self->refreshView endRefreshing];
            });
        }
    }];
}

#pragma mark - GADBannerView Delegate
-(void) addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
}

-(void)adViewDidReceiveAd:(GADBannerView *)bannerView{
    CGAffineTransform translate = CGAffineTransformMakeTranslation(20, -bannerView.bounds.size.height);
    bannerView.transform = translate;
    [UIView animateWithDuration:0.5 animations:^{
        bannerView.transform = CGAffineTransformIdentity;
    }];
}

-(void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    NSLog(@"%@",error.localizedDescription);
}


@end
