//
//  GADTableViewCell.m
//  BUGEGuide
//
//  Created by Andrew Leung on 10/22/18.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "GADTableViewCell.h"

@implementation GADTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
