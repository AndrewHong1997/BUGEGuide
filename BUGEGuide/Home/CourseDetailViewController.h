//
//  CourseDetailViewController.h
//  BUGEGuide
//
//  Created by Man Hong Leung on 8/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseDetailTableViewCell.h"
#import "CourseDetailInfoViewController.h"
#import "Firebase.h"
#import "GlobalFunctions.h"
#import "Reachability.h"

@protocol CourseDetailViewControllerDelegate;

@interface CourseDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CourseDetailInfoViewControllerDelegate,UIViewControllerPreviewingDelegate>{

}

@property (nonatomic,assign) id<CourseDetailViewControllerDelegate>delegate;
@property (retain, nonatomic) IBOutlet UITableView *courseDetailView;
@property (retain, nonatomic) IBOutlet CourseDetailTableViewCell *Acell;
@property (retain, nonatomic) CourseDetailInfoViewController *courseDetailInfoViewController;
@property (retain, nonatomic) FIRDatabaseReference *ref;
@property (retain, nonatomic) Reachability *hostReachability;

@property (retain, nonatomic) NSString *catgegory;
@property (retain, nonatomic) NSMutableDictionary *courseDict;
@property (retain, nonatomic) NSMutableArray *courseArray;

@end

@protocol CourseDetailViewControllerDelegate<NSObject>
@end


