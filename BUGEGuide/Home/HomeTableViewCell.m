//
//  HomeTableViewCell.m
//  BUGEGuide
//
//  Created by Man Hong Leung on 9/7/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
