//
//  CourseDetailInfoTableViewCell.h
//  BUGEGuide
//
//  Created by Andrew Leung on 25/8/2018.
//  Copyright © 2018 Man Hong Leung. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CourseDetailInfoTableViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UITextView *infoLabel;

@end


